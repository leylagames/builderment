﻿using System.Collections.Generic;
using UnityEngine;
// using Facebook.Unity;
// using GameAnalyticsSDK;
using GameDevUtils;
using GameDevUtils.DataManagement;
using GameDevUtils.GameProgression;

public class AnalyticsManager : SingletonPersistent<AnalyticsManager>
{

	protected override void Awake()
	{
		base.Awake();
		// if (FB.IsInitialized)
		// {
		// 	FB.ActivateApp();
		// }
		// else
		// {
		// 	//Handle FB.Init
		// 	FB.Init(() => { FB.ActivateApp(); });
		// }
		//
		// GameAnalytics.Initialize();
		GameController.OnGameStart.AddListener(LogLevelStartEvent);
		GameController.OnGameComplete.AddListener(LogLevelCompleteEvent);
		GameController.OnGameFail.AddListener(LogLevelFailedEvent);
		GameController.OnGameRestart.AddListener(LogReplayLevelEvent);
	}

	public bool isInitialized()
	{
		// if (FB.IsInitialized)
		// {
		// 	return true;
		// }
		// else
		// {
		 	return false;
		// }
	}


	void OnApplicationPause(bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus)
		{
			//app resume
			// if (FB.IsInitialized)
			// {
			// 	FB.ActivateApp();
			// }
			// else
			// {
			// 	//Handle FB.Init
			// 	FB.Init(() => { FB.ActivateApp(); });
			// }
		}
	}

	public void LogLevelCompleteEvent()
	{
		var levelNumber = DataManager.Get<int>("CurrentLevel") + 1;
		// Debug.Log($"Level Complete {levelNumber}");
		// GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "level-complete-" + (levelNumber));
		// FB.LogAppEvent("LevelComplete", levelNumber);
	}

	public void LogLevelFailedEvent()
	{
		var levelNumber = DataManager.Get<int>("CurrentLevel") + 1;
		// Debug.Log($"Level Fail {levelNumber}");
		// GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "level-fail-" + (levelNumber));
		// FB.LogAppEvent("LevelFailed", levelNumber);
	}

	public void LogLevelStartEvent()
	{
		var levelNumber = DataManager.Get<int>("CurrentLevel") + 1;
		// Debug.Log($"Level Start {levelNumber}");
		// GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "level-start-" + (levelNumber));
		// FB.LogAppEvent("LevelStart", levelNumber);
	}

	public void LogReplayLevelEvent()
	{
		var levelNumber = DataManager.Get<int>("CurrentLevel") + 1;
		// GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, "level-Replay-" + (levelNumber));
		// FB.LogAppEvent("LevelReplay", levelNumber);
	}

}