﻿using UnityEngine;


namespace GameDevUtils.CameraController
{


	[CreateAssetMenu(fileName = "FixedCamera", menuName = "GameDevUtils/Camera/FixedCamera")]
	public class FixedCamera : CameraState
	{

		[SerializeField] protected Vector3 posOffset, lookOffset;

		protected override void UpdateCamera(Transform camera, Transform pivot, Transform target, float deltaTime)
		{
			camera.position = target.position;
			camera.rotation = target.rotation;
			pivot.localPosition = posOffset;
			pivot.localRotation = Quaternion.Euler(lookOffset);
		}

	}


}