using GameDevUtils.CameraController;
using UnityEngine;

[CreateAssetMenu(fileName = "ShooterCamera", menuName = "GameDevUtils/Camera/ShooterCamera")]
public class ShooterCamera : CameraState
{

	[SerializeField] protected     Vector3 posOffset, lookOffset;
	[SerializeField, Range(1, 20)] float   followSpeed = 5;
	[SerializeField, Range(1, 20)] float   lookSpeed   = 5;

	protected override void UpdateCamera(Transform camera, Transform pivot, Transform target, float deltaTime)
	{
		camera.position     = target.position;
		camera.rotation     = target.rotation;
		pivot.localPosition = posOffset;
		pivot.rotation      = Quaternion.LookRotation((target.position - pivot.position).normalized) * Quaternion.Euler(lookOffset);
	}

}