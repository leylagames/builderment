namespace ChainOfResponsibility
{


	public abstract class BaseLink : ILink
	{

		protected ILink nextLink;


		public virtual object Handle(object request)
		{
			return nextLink?.Handle(request);
		}

		public ILink SetNext(ILink nextLink)
		{
			this.nextLink = nextLink;
			return nextLink;
		}

	}


	public interface ILink
	{

		object Handle(object request);

		ILink SetNext(ILink nextLink);

	}


}