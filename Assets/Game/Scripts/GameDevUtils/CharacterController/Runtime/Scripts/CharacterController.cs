using System;
using DG.Tweening;
using GameDevUtils.GameProgression;
using GameDevUtils.HealthSystem;
using GameDevUtils.InteractSyetem;
using UnityEngine;
using UnityEngine.UI;


namespace GameDevUtils.CharacterController2
{


	public class CharacterController : GameDevBehaviour, IEffectContainer, IDamageable
	{

		[SerializeField] CharacterBase character;
		[SerializeField] HealthSystem.HealthSystem healthSystem;
		IEffectContainer effectContainer;
		public bool IsDestroyed { get; set; }


		void Awake()
		{
			effectContainer = new EffectContainer();
			character.Init(this);
			GameController.OnGameStart.AddListener(() => { character.CanControl = true; });
		
			GameController.OnCalculateResult.AddListener((() =>
			{
				enabled = false;character.CanControl = false;
				Animator.SetFloat("Type", 1);
				Animator.SetFloat("Value", 2);
			}));
			
			// GameController.OnGameComplete.AddListener(() =>
			// {
			// 	enabled = false;character.CanControl = false;
			// 	Animator.SetFloat("Type", 1);
			// 	Animator.SetFloat("Value", 2);
			// 	
			// });
			AddEffect("Burnable", new ActionEffect(() =>
			{
				character.CanControl = false;
				enabled = false;
				Animator.SetTrigger("DoAction");
				// Animator.SetFloat("Type", 1);
				// Animator.SetFloat("Value", 1);
			}));
			AddEffect("FinishLine", new ActionEffect(() =>
			{
				character.CanControl = false;
				Animator.SetFloat("Value", 0);
				Animator.SetFloat("Type", 1);
				GameController.ChangeProgression.Send(GameProgression.GameProgression.Completed);
			}));
		}


		public void AddEffect(string id, IEffect effect)
		{
			effectContainer.AddEffect(id, effect);
		}

		public void EmitEffect(IEffector effect)
		{
			effectContainer.EmitEffect(effect);
		}

		public void EmitEffect(string effectId)
		{
			effectContainer.EmitEffect(effectId);
		}


		public void Damage(float damageAmount, Vector3 hitPoint)
		{
			healthSystem.TakeDamage(damageAmount, hitPoint);
		}

		public void DestroyObject()
		{
			healthSystem.Death();
		}

	}


	public class CharacterAddon
	{

	}


}