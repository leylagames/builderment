﻿using System;
using UnityEngine;


namespace GameDevUtils.Detections
{


	public class CollisionDetector : MonoBehaviour
	{

		public Action<Collision> enter, stay, exit;

		private void OnCollisionEnter(Collision other)
		{
			enter?.Invoke(other);
		}

		private void OnCollisionStay(Collision other)
		{
			stay?.Invoke(other);
		}

		private void OnCollisionExit(Collision other)
		{
			exit?.Invoke(other);
		}

	}

	public static class CollisionExtensions
	{

		public static void OnCollisionEnter(this Collider col, Action<Collision> trigger)
		{
			CollisionDetector trgr = col.GetComponent<CollisionDetector>();
			if (trgr == null)
			{
				trgr = col.gameObject.AddComponent<CollisionDetector>();
			}

			trgr.hideFlags = HideFlags.HideInInspector;
			trgr.enter     = trigger;
		}

		public static void OnCollisionStay(this Collider col, Action<Collision> trigger)
		{
			CollisionDetector trgr = col.GetComponent<CollisionDetector>();
			if (trgr == null)
			{
				trgr = col.gameObject.AddComponent<CollisionDetector>();
			}

			trgr.hideFlags = HideFlags.HideInInspector;
			trgr.stay      = trigger;
		}

		public static void OnCollisionExit(this Collider col, Action<Collision> trigger)
		{
			CollisionDetector trgr = col.GetComponent<CollisionDetector>();
			if (trgr == null)
			{
				trgr = col.gameObject.AddComponent<CollisionDetector>();
			}

			trgr.hideFlags = HideFlags.HideInInspector;
			trgr.exit      = trigger;
		}

	}


}