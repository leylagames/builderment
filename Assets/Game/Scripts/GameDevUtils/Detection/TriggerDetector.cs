﻿using System;
using UnityEngine;
namespace GameDevUtils.Detections
{
    public class TriggerDetector : MonoBehaviour
    {
        public Action<Collider> enter, stay, exit;
        private void OnTriggerEnter (Collider other)
        {
            enter?.Invoke (other);
        }
        private void OnTriggerStay (Collider other)
        {
            stay?.Invoke (other);
        }

        private void OnTriggerExit (Collider other)
        {
            exit?.Invoke (other);
        }
    }

    public static class TriggerExtensions
    {
        public static void OnTriggerEnter (this Collider col, Action<Collider> trigger)
        {
            TriggerDetector trgr = col.GetComponent<TriggerDetector> ();
            if (trgr == null)
            {
                trgr = col.gameObject.AddComponent<TriggerDetector> ();
            }
            trgr.hideFlags = HideFlags.HideInInspector;
            trgr.enter = trigger;
        }
        public static void OnTriggerStay (this Collider col, Action<Collider> trigger)
        {
            TriggerDetector trgr = col.GetComponent<TriggerDetector> ();
            if (trgr == null)
            {
                trgr = col.gameObject.AddComponent<TriggerDetector> ();
            }
            trgr.hideFlags = HideFlags.HideInInspector;
            trgr.stay = trigger;
        }

        public static void OnTriggerExit (this Collider col, Action<Collider> trigger)
        {
            TriggerDetector trgr = col.GetComponent<TriggerDetector> ();
            if (trgr == null)
            {
                trgr = col.gameObject.AddComponent<TriggerDetector> ();
            }
            trgr.hideFlags = HideFlags.HideInInspector;
            trgr.exit = trigger;
        }
    }
}