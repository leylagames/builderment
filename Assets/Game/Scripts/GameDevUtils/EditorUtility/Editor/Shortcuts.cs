using System.Linq;
using GameDevUtils.DataBase;
using GameDevUtils.Settings;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class Shortcuts : MonoBehaviour
{

	[MenuItem("Tools/GameDevUtils/Game/Open Main Scene %&s")]
	public static void OpenMainGameScene()
	{
		var scenesGUIDs = AssetDatabase.FindAssets("t:Scene");
		var scenesPaths = scenesGUIDs.Select(AssetDatabase.GUIDToAssetPath);
		foreach (string scenesPath in scenesPaths)
		{
			if (scenesPath.Contains(GameSettings.Instance.general.GameSceneName))
			{
				EditorSceneManager.OpenScene(scenesPath);
				break;
			}
		}
	}

	[MenuItem("Tools/GameDevUtils/Game/Game Settings %&g")]
	public static void OpenMainGameSettings()
	{
		Selection.activeObject = GameSettings.Instance;
	}

	[MenuItem("Tools/GameDevUtils/Game/Game Database %&d")]
	public static void OpenMainGameDatabase()
	{
		Selection.activeObject = GameDataBase.Instance;
	}

}