using System;
using GameDevUtils.DataManagement;
using GameDevUtils.SceneManagement;
using UnityEngine;


namespace GameDevUtils.GameProgression
{


	public enum GameProgression
	{

		Loaded,
		Started,
		Paused,
		CalculateResult,
		Completed,
		Failed

	}

	public class GameController : SingletonLocal<GameController>
	{

		[SerializeField] GameProgression gameProgression = GameProgression.Loaded;
		public GameProgression Progression
		{
			get => gameProgression;
			set
			{
				gameProgression = value;
				OnProgressionChange.Send(gameProgression);
			}
		}
		public bool isPlayingMode = false;
		//Events
		public static readonly EventListener<GameProgression> ChangeProgression = new EventListener<GameProgression>();
		public static readonly EventListener OnGameStart = new EventListener();
		public static readonly EventListener OnGamePause = new EventListener();
		public static readonly EventListener OnGameRestart = new EventListener();
		public static readonly EventListener OnGameReloadScene = new EventListener();
		public static readonly EventListener OnGameComplete = new EventListener();
		public static readonly EventListener OnGameFinish = new EventListener();
		public static readonly EventListener OnGameFail = new EventListener();
		public static readonly EventListener OnCalculateResult = new EventListener();
		// public static readonly EventListener OnNextResult = new EventListener();

		public static readonly EventListener<GameProgression> OnProgressionChange = new EventListener<GameProgression>();


		bool isReloadingScene;

		protected override void Awake()
		{
			base.Awake();
			ChangeProgression.AddListener(ChangeProgressionListener);
			OnGameStart.AddListener(StartGame);
			OnGameRestart.AddListener(Restart);
			OnGameReloadScene.AddListener(ReloadScene);
			OnGamePause.AddListener(PauseGame);
			OnProgressionChange.AddListener(AfterProgressUpdate);
			Application.targetFrameRate = 60;
		}


		void Start()
		{
			OnGameStart.Send();
		}

		void Restart()
		{
			OnGameReloadScene.Send();
		}

		void ReloadScene()
		{
			if (isReloadingScene) return;
			isReloadingScene = true;
			SceneLoader.ReLoadScene();
		}

		void ChangeProgressionListener(GameProgression progression)
		{
			Progression = progression;
		}

		void StartGame()
		{
			isPlayingMode = true;
		}

		void PauseGame()
		{
			Time.timeScale = 1;
		}


		void AfterProgressUpdate(GameProgression progression)
		{
			switch (progression)
			{
				case GameProgression.Loaded:
					break;

				case GameProgression.Started:
					OnGameStart.Send();
					break;

				case GameProgression.Paused:
					OnGameRestart.Send();
					break;

				case GameProgression.Completed:
					OnGameComplete.Send();
					OnGameFinish.Send();
					break;

				case GameProgression.Failed:
					OnGameFail.Send();
					OnGameFinish.Send();
					break;

				case GameProgression.CalculateResult:
					OnCalculateResult.Send();
					OnGameFinish.Send();
					break;
			}
		}

		void OnDestroy()
		{
			ChangeProgression.RemoveAllListener();
			OnGameStart.RemoveAllListener();
			OnGamePause.RemoveAllListener();
			OnGameComplete.RemoveAllListener();
			OnGameFail.RemoveAllListener();
			OnGameRestart.RemoveAllListener();
			OnProgressionChange.RemoveAllListener();
			OnCalculateResult.RemoveAllListener();
			OnGameFinish.RemoveAllListener();
			// OnNextResult.RemoveAllListener();
			DataManager.Persist();
			
			// DataManager.DeleteAllData();
		}

	}


}