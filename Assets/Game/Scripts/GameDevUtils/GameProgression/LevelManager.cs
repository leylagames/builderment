﻿using System;
// using GameAnalyticsSDK;
using GameDevUtils.DataManagement;
using GameDevUtils.Settings;
using UnityEngine;


namespace GameDevUtils.GameProgression
{


	public class LevelManager : SingletonLocal<LevelManager>
	{

		[Range(1, 10), SerializeField] int startLevel = 1;
		[SerializeField] Level[] levels;
		public ILevel currentLevel;


		bool debugOn => GameSettings.Instance.general.DebugMode;
		int currentLevelIndex
		{
			get => DataManager.Get<int>("CurrentLevel");
			set => DataManager.Save("CurrentLevel", value);
		}

		protected override void Awake()
		{
			base.Awake();
			SetupLevel();
			GameController.OnGameComplete.AddListener(CompleteLevel);
			GameController.OnGameFail.AddListener(LevelFail);
		}

		void SetupLevel()
		{
			if (debugOn)
				startLevel -= 1;
			else
			{
				for (int i = 0; i < levels.Length; i++)
				{
					ILevel level = levels[i];
					if (level.isCompleted) continue;
					startLevel = i;
					break;
				}

				if (startLevel != currentLevelIndex)
					startLevel = currentLevelIndex;
			}

			if (startLevel >= levels.Length)
			{
				startLevel %= levels.Length;
			}

			currentLevel = levels[startLevel];
			currentLevel.Initialize();
		
		}

		void CompleteLevel()
		{
			currentLevel.Complete();
			currentLevelIndex++;
		}

		void LevelFail()
		{
		}


		#region EditorStuff

		[ContextMenu("Create New")]
		private void CreateNewLevel()
		{
			GameObject newLevel = new GameObject("Level", typeof(NormalLevel)) {transform = {parent = transform}};
			var normalLevel = newLevel.GetComponent<NormalLevel>();
			normalLevel.Validate(levels.Length + 1);
			Array.Resize(ref levels, levels.Length + 1);
			levels[levels.Length - 1] = normalLevel;
		}

		[ContextMenu("Duplicate Last")]
		private void DuplicateOldOne()
		{
			#if UNITY_EDITOR
			var normalLevel = (Level) UnityEditor.PrefabUtility.InstantiatePrefab(levels[levels.Length - 1], transform);
			normalLevel.Validate(levels.Length + 1);
			Array.Resize(ref levels, levels.Length + 1);
			levels[levels.Length - 1] = normalLevel;
			#endif
		}

		[ContextMenu("Update Levels")]
		private void UpdateLevels()
		{
			levels = GetComponentsInChildren<Level>(true);
			for (int i = 0; i < levels.Length; i++)
			{
				levels[i].Validate(i + 1);
				levels[i].gameObject.SetActive(false);
			}
		}

		[ContextMenu("Next Level")]
		public void NextLevel()
		{
			currentLevelIndex++;
			GameController.OnGameRestart.Send();
		}

		#endregion

	}


	public abstract class Level : MonoBehaviour, ILevel
	{

		[SerializeField] int m_LevelNo;
		[SerializeField] LevelType m_NextLevel;
		public int levelNo => m_LevelNo;
		public LevelType nextLevel => m_NextLevel;
		public bool isCompleted => DataManager.Get<int>(levelName) == 1;

		public string levelName => $"Level {levelNo}";


		public abstract void Initialize();


		public virtual void Complete()
		{
			DataManager.Save(levelName, 1);
		}

		public void Validate(int no)
		{
			m_LevelNo = no;
			gameObject.name = levelName;
		}

	}

	public interface ILevel
	{

		int levelNo { get; }
		string levelName { get; }
		LevelType nextLevel { get; }
		bool isCompleted { get; }

		void Initialize();

		void Complete();

		void Validate(int no);

	}

	public enum LevelType
	{

		NormalLevel,
		SceneSwitchLevel

	}


}