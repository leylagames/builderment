﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDevUtils.Coroutine;
using UMGS.WayPointSystem;
using UnityEngine;


namespace GameDevUtils.GameProgression
{


	public class NormalLevel : Level
	{

		[SerializeField] public WayPointManager path;
		[SerializeField] List<GameObject> references;
		public List<int> requireIds = new List<int>();
		public List<int> stolenElementIds = new List<int>();
		public float levelWinPercentage;

		public override void Initialize()
		{
			gameObject.SetActive(true);
		}

		public void OnPathUpdate(int pathIndex)
		{
			for (var i = 0; i < references.Count; i++)
			{
				references[i].SetActive(i == pathIndex - 1);
			}
		}

		public bool ValidateLevel()
		{
			bool isAllRequiredElementsStolen = true;
			int noOfRequireStolenElement = 0;
			stolenElementIds = stolenElementIds.Distinct().ToList();
			for (int i = 0; i < requireIds.Count; i++)
			{
				if (!stolenElementIds.Contains(requireIds[i]))
				{
					isAllRequiredElementsStolen = false;
				}
				else
				{
					noOfRequireStolenElement++;
				}
			}

			levelWinPercentage = (float) noOfRequireStolenElement / requireIds.Count;
			levelWinPercentage = Mathf.Clamp01(levelWinPercentage);
			return isAllRequiredElementsStolen;
		}

	}


}