using System;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor.Build.Reporting;
using UnityEditor;
using UnityEditor.SceneManagement;
#endif


namespace GameDevUtils.Settings
{


	[System.Serializable]
	public class Build
	{

		public string buildPath = $"C:/Users/Umair Saifullah/Videos/Backup/";
		#if UNITY_EDITOR
		public string BuildAndroidProductName
		{
			get { return $"{PlayerSettings.productName}_v{GameSettings.Instance.general.BuildVersion}_{GameSettings.Instance.general.BuildNumber}_{PlayerSettings.GetScriptingBackend(BuildTargetGroup.Android)}_{PlayerSettings.Android.targetArchitectures.ToString().Replace(",", "_")}_{(buildOptionAndroid.HasFlag(BuildOptions.Development) ? "Dev" : "Prod")}.{(IsAppBundle ? "aab" : "apk")}"; }
		}
		public string BuildAndroidFolderPath
		{
			get { return $"{buildPath}{PlayerSettings.productName}_v{GameSettings.Instance.general.BuildVersion}_{GameSettings.Instance.general.BuildNumber}_Android/"; }
		}
		
		[field:SerializeField]
		public string BuildAndroidFullPathIncludingFolder
		{
			get { return $"{BuildAndroidFolderPath}{BuildAndroidProductName}"; }
		}

		public BuildOptions buildOptionAndroid = BuildOptions.AutoRunPlayer | BuildOptions.ShowBuiltPlayer;
		#endif
		public bool IsAppBundle = false;

		public void DoBuild()
		{
			#if UNITY_EDITOR
			EditorSceneManager.SaveOpenScenes();
			if (!Directory.Exists(buildPath))
			{
				Directory.CreateDirectory(buildPath);
			}

			EditorUserBuildSettings.buildAppBundle = IsAppBundle;
			EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
			EditorUserBuildSettings.installInBuildFolder = true;
			EditorUserBuildSettings.SetBuildLocation(BuildTarget.Android, buildPath);
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes);
			buildPlayerOptions.locationPathName = BuildAndroidFullPathIncludingFolder;
			buildPlayerOptions.target = BuildTarget.Android;
			buildPlayerOptions.options = BuildOptions.CompressWithLz4HC | buildOptionAndroid;
			BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
			;
			BuildSummary summary = report.summary;
			if (summary.result == BuildResult.Succeeded)
			{
				UnityEngine.Debug.LogError($"BuildState: Build succeeded: {summary.totalSize} bytes File: {BuildAndroidFullPathIncludingFolder}");
				GameSettings.Instance.general.OnBuildSuccess();
			}

			if (summary.result == BuildResult.Failed)
			{
				UnityEngine.Debug.LogError("BuildState: Build failed");
			}

			#endif
		}

	}


}