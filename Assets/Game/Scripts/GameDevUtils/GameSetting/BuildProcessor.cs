#if UNITY_EDITOR
using GameDevUtils.Settings;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;


namespace GameDevUtils.Build
{


	public class BuildProcessor : IPreprocessBuildWithReport
	{

		public int callbackOrder => 0;

		public void OnPreprocessBuild(BuildReport report)
		{
			GameSettings.Instance.Sync();
		}

	}


}


#endif