using MoreMountains.NiceVibrations;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
#endif
using UnityEngine;
using Object = UnityEngine.Object;


namespace GameDevUtils.Settings
{


	[CreateAssetMenu(menuName = "GameDevUtils/Settings")]
	public class GameSettings : SingletonScriptable<GameSettings>
	{

		public General general;
		public Build build;
		public Note note;


		public void Sync()
		{
			#if UNITY_EDITOR
			general.UpdateQuality();
			PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, general.BundleId);
			PlayerSettings.companyName = "Leyla";
			PlayerSettings.productName = general.GameName;
			PlayerSettings.bundleVersion = general.BuildVersion;
			PlayerSettings.Android.bundleVersionCode = general.BuildNumber;
			PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Android, new Texture2D[] {general.GameIcon.texture});
			#endif
		}

	}


	#if UNITY_EDITOR


	[CustomEditor(typeof(GameSettings))]
	public class GameSettingEditor : Editor
	{

		GameSettings settings;
		int tab = 0;

		public override void OnInspectorGUI()
		{
			if (settings == null)
			{
				settings = (GameSettings) target;
			}

			tab = GUILayout.Toolbar(tab, new string[] {"General", "Build", "Note"});
			switch (tab)
			{
				case 0:
					var generalProperty = serializedObject.FindProperty("general");
					EditorGUILayout.PropertyField(generalProperty);
					EditorGUILayout.Space();
					if (GUILayout.Button("Open Quality Settings"))
					{
						Selection.activeObject = Unsupported.GetSerializedAssetInterfaceSingleton("QualitySettings");
					}

					if (GUILayout.Button("Sync"))
					{
						settings.Sync();
					}

					break;

				case 1:
					var buildProperty = serializedObject.FindProperty("build");
					EditorGUILayout.PropertyField(buildProperty);
					EditorGUILayout.LabelField(GameSettings.Instance.build.BuildAndroidProductName);
					EditorGUILayout.Space();
					if (GUILayout.Button("Build Android"))
					{
						GameSettings.Instance.build.DoBuild();
					}

					break;

				case 2:
					var noteProperty = serializedObject.FindProperty("note");
					EditorGUILayout.PropertyField(noteProperty);
					EditorGUILayout.Space();
					if (GUILayout.Button("Open Link"))
					{
					}

					break;
			}

			serializedObject.ApplyModifiedProperties();
		}

	}
	#endif


}