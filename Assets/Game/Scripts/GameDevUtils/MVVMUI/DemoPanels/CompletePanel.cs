using GameDevUtils.GameProgression;
using GameDevUtils.SceneManagement;
using GameDevUtils.MVVM;


namespace GameDevUtils.UI
{


	public class CompletePanel : UIPanel
	{

		public override void Init(IStateMachine stateMachine)
		{
			base.Init(stateMachine);
			EventBinder("NextButton", RestartButton);
		}

		public override void Enter()
		{
			base.Enter();
			// SoundManager.Instance.Play("Success");
		}

		void RestartButton()
		{
			GameController.OnGameReloadScene.Send();
		}

	}


}