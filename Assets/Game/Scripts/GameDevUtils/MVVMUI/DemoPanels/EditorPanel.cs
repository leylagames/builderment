using System;
using GameDevUtils.DataManagement;
using GameDevUtils.GameProgression;
using GameDevUtils.MVVM;


namespace GameDevUtils.UI
{


	public class EditorPanel : UIPanel
	{

		InputEditEvents inputEditEvents;

		public override void Init(IStateMachine stateMachine)
		{
			base.Init(stateMachine);
			GameController.OnGameStart.AddListener(OnGameStart);
		}

		public override void Enter()
		{
			base.Enter();
			ActiveMoveControl(false);
		}

		void OnGameStart()
		{
			if (inputEditEvents == null)
			{
				inputEditEvents = storageService.Load<InputEditEvents>();
				inputEditEvents.onSelect.AddListener(OnMoveButton);
				inputEditEvents.onSave.AddListener(ExitState);
				inputEditEvents.onCancel.AddListener(ExitState);
				EventBinder("Delete", inputEditEvents.onDelete.Send);
				EventBinder("RotateLeft", inputEditEvents.onRotateLeft.Send);
				EventBinder("RotateRight", inputEditEvents.onRotateRight.Send);
				EventBinder("Move", inputEditEvents.onMove.Send);
				// EventBinder("Select", inputEditEvents.onSelect.Send);
				EventBinder("Save", inputEditEvents.onSave.Send);
				EventBinder("Cancel", inputEditEvents.onCancel.Send);
			}
		}

		void ExitState()
		{
			stateMachine.ExitAnyStates();
		}

		void OnMoveButton(int index)
		{
			ActiveMoveControl(true);
		}

		void ActiveMoveControl(bool value)
		{
			views["EditorControl"].GetTransform().SetActive(!value);
			views["MoveControl"].GetTransform().SetActive(value);
		}

	}


}