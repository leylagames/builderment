using GameDevUtils.GameProgression;
using GameDevUtils.MVVM;


namespace GameDevUtils.UI
{


	public class FailPanel : UIPanel
	{

		public override void Init(IStateMachine stateMachine)
		{
			base.Init(stateMachine);
			EventBinder("RestartButton", Restart);
		}


		void Restart()
		{
			GameController.OnGameReloadScene.Send();
		}

	}


}