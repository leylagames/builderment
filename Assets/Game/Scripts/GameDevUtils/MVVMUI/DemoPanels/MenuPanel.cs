﻿using GameDevUtils.GameProgression;
using GameDevUtils.MVVM;
using UnityEngine;


namespace GameDevUtils.UI
{


	public class MenuPanel : UIPanel
	{

		GameObject bottomPanel;
		InputEditEvents inputEditEvents;

		public override void Init(IStateMachine stateMachine)
		{
			base.Init(stateMachine);
			bottomPanel = views["BottomPanel"].GetTransform().gameObject;
			var setting = new Transition(nameof(SettingPanel));
			var inventory = new Transition(nameof(InventoryPanel));
			EventBinder("SettingButton", () => SetTransitionCondition(nameof(SettingPanel), true));
			EventBinder("ConstructButton", (ConstructButtonClick));
			GameController.OnGameStart.AddListener(InputEvents);
			// var levelFail = new Transition(nameof(FailPanel));
			SetTransitions(inventory, setting);
		}

		public override void Enter()
		{
			base.Enter();
			// SetTransitionCondition(nameof(FailPanel), true);
		}

		void ConstructButtonClick()
		{
			bottomPanel.SetActive(false);
			stateMachine.AnyTransition(stateMachine.GetState(nameof(InventoryPanel)));
		}

		void InputEvents()
		{
			if (inputEditEvents == null)
			{
				inputEditEvents = storageService.Load<InputEditEvents>();
				inputEditEvents.onCancel.AddListener(() => bottomPanel.SetActive(true));
				inputEditEvents.onSave.AddListener(() => bottomPanel.SetActive(true));
				inputEditEvents.onSelect.AddListener(ActiveEditorPanel);
			}
		}

		void ActiveEditorPanel(int selectedItem)
		{
			bottomPanel.SetActive(false);
			stateMachine.ExitAnyStates();
			stateMachine.AnyTransition(stateMachine.GetState(nameof(EditorPanel)));
		}

	}


}