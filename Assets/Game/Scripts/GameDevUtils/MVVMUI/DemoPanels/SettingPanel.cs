using GameDevUtils.DataManagement;
using GameDevUtils.MVVM;
using MoreMountains.NiceVibrations;


namespace GameDevUtils.UI
{


	public class SettingPanel : UIPanel
	{

		int soundValue
		{
			get => DataManager.Get<int>("ToggleSound");
			set => DataManager.Save("ToggleSound", value);
		}
		int hapticValue
		{
			get => DataManager.Get<int>("ToggleHaptic");
			set => DataManager.Save("ToggleHaptic", value);
		}

		public override void Init(IStateMachine stateMachine)
		{
			base.Init(stateMachine);
		//	SetTransitions(new Transition(nameof(GamePlayPanel)));
		}

		public override void InitViewModel()
		{
			base.InitViewModel();
			EventBinder("CrossButton", () => SetTransitionCondition(nameof(EditorPanel), true));
			EventBinder("ToggleHaptic", ToggleHaptic);
			EventBinder("ToggleSound", ToggleSound);
			InitializeSettings();
		}

		void InitializeSettings()
		{
			Vibration.Active = hapticValue == 0;
			if (SoundSystem.SoundManager.Instance) SoundSystem.SoundManager.Instance.SetMute(soundValue == 0);
		}

		void ToggleHaptic()
		{
			if (hapticValue == 0)
			{
				hapticValue = 1;
				Vibration.Active = true;
			}
			else
			{
				hapticValue = 0;
				Vibration.Active = false;
			}
		}

		void ToggleSound()
		{
			if (soundValue == 0)
			{
				soundValue = 1;
				if (SoundSystem.SoundManager.Instance)
					SoundSystem.SoundManager.Instance.SetMute(true);
			}
			else
			{
				soundValue = 0;
				if (SoundSystem.SoundManager.Instance)
					SoundSystem.SoundManager.Instance.SetMute(false);
			}
		}

	}

	public class Vibration
	{

		public static bool Active = false;

		public static void Haptic(HapticTypes hapticType, bool defaultToRegularVibrate = false, bool allowVibrationOnLegacyDevices = true)
		{
			if (Active)
				MMVibrationManager.Haptic(hapticType, defaultToRegularVibrate, allowVibrationOnLegacyDevices);
		}

	}


}