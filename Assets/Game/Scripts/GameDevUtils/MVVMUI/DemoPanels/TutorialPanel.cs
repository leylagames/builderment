﻿using System;
using System.Linq;
using ChainOfResponsibility;
using GameDevUtils.UI;
using UnityEngine;

public class TutorialPanel : UIPanel
{

	public        TutorialLink[]       sequence;
	public static Func<object, object> handleReceiver;
	public        TutorialLink         currentInComplete => sequence.FirstOrDefault(tutorialHandler => !tutorialHandler.isCompleted);


	public override void InitViewModel()
	{
		base.InitViewModel();
		if (sequence.Length < 1) return;
		for (int i = 0; i < sequence.Length; i++)
		{
			sequence[i].SetNext(sequence[i + 1]);
		}

		handleReceiver += sequence[0].Handle;
		
	}

}

[System.Serializable]
public class TutorialLink : BaseLink
{

	public string id;
	public bool isCompleted
	{
		get => PlayerPrefs.GetInt(id, 0) == 1;
		set => PlayerPrefs.SetInt(id, value ? 1 : 0);
	}

	public override object Handle(object request)
	{
		if (request as string == id && !isCompleted)
		{
			isCompleted = true;
			return true;
		}

		return nextLink.Handle(request);
	}

}