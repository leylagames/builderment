﻿using System;
using GameDevUtils.MVVM;
using TMPro;
using UnityEngine;


namespace GameDevUtils.UI
{


	public class AdvanceButtonBinder : Intractable, IView
	{

		protected TextMeshProUGUI text;
		public    string          Id        => gameObject.name;
		public    IViewModel      viewModel { get; set; }

		public virtual void Init(IViewModel viewModel)
		{
			this.viewModel = viewModel;
			if (!text)
			{
				text = GetComponentInChildren<TextMeshProUGUI>();
				if (text && string.IsNullOrEmpty(text.text)) text.text = Id;
			}

			viewModel.eventBinder  += ClickEvent;
			viewModel.stringBinder += TextUpdater;
		}


		void TextUpdater(string id, string value)
		{
			if (id == Id)
			{
				text.text = value;
			}
		}

		protected virtual void ClickEvent(string id, Action click)
		{
			if (id == $"{Id}")
			{
				onClick.AddListener(click.Invoke);
			}
			else if (id == $"{Id}Up")
			{
				onUp.AddListener(click.Invoke);
			}
			else if (id == $"{Id}Down")
			{
				onDown.AddListener(click.Invoke);
			}
			else if (id == $"{Id}Hold")
			{
				onHold.AddListener(click.Invoke);
			}
		}

		public Transform GetTransform() => transform;

		public void Active(bool value) => gameObject.SetActive(value);

	}


}