﻿using DG.Tweening;
using GameDevUtils.MVVM;
using GameDevUtils.Tweening;
using UnityEngine;


namespace GameDevUtils.UI
{


	[RequireComponent(typeof(IAnimation))]
	public class AnimatedButton : AdvanceButtonBinder
	{

		private IAnimation animation;

		public override void Init(IViewModel viewModel)
		{
			base.Init(viewModel);
			animation = GetComponent<IAnimation>();
		}

		protected override void Press()
		{
			if (!IsActive())
				return;
			animation?.Play();
		}

		protected override void Unpress()
		{
			if (!IsActive())
				return;
			animation?.Reverse();
		}

	}


}