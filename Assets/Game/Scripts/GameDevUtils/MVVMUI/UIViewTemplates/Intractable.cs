using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace GameDevUtils.UI
{


}


[RequireComponent(typeof(UnityEngine.UI.Image))]
public class Intractable : UIBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler, IPointerClickHandler
{

	[SerializeField]                                                               public UnityEvent onClick = new UnityEvent();
	[SerializeField]                                                               public UnityEvent onUp    = new UnityEvent();
	[SerializeField]                                                               public UnityEvent onDown  = new UnityEvent();
	[SerializeField] [Tooltip("It will update every frame when button is pressed")] public UnityEvent onHold  = new UnityEvent();

	protected bool m_pointerInside  = false;
	protected bool m_pointerPressed = false;


	void Update()
	{
		if (m_pointerPressed)
		{
			onHold?.Invoke();
		}
	}


	public virtual void OnPointerEnter(PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Left)
			return;
		m_pointerInside = true;
		if (m_pointerPressed)
			Press();
	}

	public virtual void OnPointerExit(PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Left)
			return;
		m_pointerInside = false;
		Unpress();
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Left)
			return;
		m_pointerPressed = false;
		Unpress();
		if (m_pointerInside)
			onUp?.Invoke();
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		if (eventData.button != PointerEventData.InputButton.Left)
			return;
		onDown?.Invoke();
		m_pointerPressed = true;
		if (m_pointerPressed) Press();
	}

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		if (m_pointerInside)
		{
			onClick.Invoke();
		}
	}

	protected virtual void Press()
	{
	}

	protected virtual void Unpress()
	{
	}

}