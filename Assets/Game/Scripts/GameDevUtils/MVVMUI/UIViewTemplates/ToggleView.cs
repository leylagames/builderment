using System;
using GameDevUtils.DataManagement;
using GameDevUtils.MVVM;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace GameDevUtils.UI
{


	public class ToggleView : MonoBehaviour, IPointerDownHandler, IView
	{

		[SerializeField] UnityEvent On, Off;
		[SerializeField] UnityEvent clickEvent;
		public           string     Id        => gameObject.name;
		public           IViewModel viewModel { get; set; }
		bool                        toggleValue = false;

		public void Init(IViewModel viewModel)
		{
			viewModel.eventBinder += OnClick;
			toggleValue           =  DataManager.Get<int>(Id) == 0;
			if (toggleValue)
			{
				On.Invoke();
			}
			else Off.Invoke();
		}

		void OnClick(string id, Action action)
		{
			if (id == this.Id)
			{
				clickEvent?.AddListener(action.Invoke);
			}
		}

		public Transform GetTransform() => transform;

		public void Active(bool value) => gameObject.SetActive(value);

		public void OnPointerDown(PointerEventData eventData)
		{
			clickEvent.Invoke();
			toggleValue = !toggleValue;
			if (toggleValue)
			{
				On.Invoke();
			}
			else Off.Invoke();
		}

	}


}