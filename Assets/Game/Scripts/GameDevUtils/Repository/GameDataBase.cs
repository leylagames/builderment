using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Object = UnityEngine.Object;


namespace GameDevUtils.DataBase
{


	[Serializable]
	public class GenericCategory<T> : IEnumerable<T>
	{

		public string name;
		public List<T> elements = new List<T>();

		public IEnumerator<T> GetEnumerator()
		{
			return elements.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

	}

	[Serializable]
	public class GameObjectCategory : GenericCategory<GameObject>
	{

	}


	[Serializable]
	public class PoolElement
	{

		public string nameId;
		public GameObject prefab;
		public int noOfElement = 1;
		public bool prebaked = false;

	}

	[Serializable]
	public class PoolCategory : IEnumerable<PoolElement>
	{

		public List<PoolElement> elements = new List<PoolElement>();

		public IEnumerator<PoolElement> GetEnumerator()
		{
			return elements.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

	}

	[Serializable]
	public class OutlineSettings
	{

		[SerializeField] public Outline.Mode outlineMode;

		[SerializeField] public Color outlineColor = Color.white;

		[SerializeField, Range(0f, 10f)] public float outlineWidth = 2f;

	}

	[Serializable]
	public class GridSettings
	{

		public int width = 10, length = 10;
		public float cellSize = 10f;

	}

	[Serializable]
	public class InventoryCategory : GenericCategory<Item>
	{

	}

	[Serializable]
	public class PlaceableContainer : IEnumerable<Placeable>
	{

		public List<Placeable> placeables = new List<Placeable>();

		public Placeable this[int index] => placeables[index];

		public int IndexOf(Placeable placeable)
		{
			return placeables.IndexOf(placeable);
		}

		public IEnumerator<Placeable> GetEnumerator()
		{
			return placeables.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

	}

	[CreateAssetMenu(menuName = "GameDevUtils/GameDataBase")]
	public class GameDataBase : SingletonScriptable<GameDataBase>
	{

		public List<GameObjectCategory> m_Categories = new List<GameObjectCategory>();
		public PoolCategory poolCategory = new PoolCategory();
		public OtherParameters otherParameters;
		public OutlineSettings outlineSettings;
		public GridSettings gridSettings;
		public PlaceableContainer placeableContainer;
		public List<InventoryCategory> inventoryCategory = new List<InventoryCategory>();

		public GameObject InstantiateGameObject(string categoryName, string objectName)
		{
			GameObject gameObject = null;
			foreach (GameObjectCategory category in m_Categories)
			{
				if (category.name == categoryName)
				{
					for (var i = 0; i < category.elements.Count; i++)
					{
						if (category.elements[i].gameObject.name == objectName)
						{
							gameObject = category.elements[i];
							break;
						}
					}
				}
			}

			if (gameObject != null)
				return Object.Instantiate(gameObject);
			return null;
		}

		public GameObject InstantiateGameObject(string categoryName, string objectName, Vector3 position, Quaternion rotation, Transform parent)
		{
			GameObject gameObject = InstantiateGameObject(categoryName, objectName);
			gameObject.transform.position = position;
			gameObject.transform.rotation = rotation;
			gameObject.transform.parent = parent;
			return gameObject;
		}

	}

	#if UNITY_EDITOR
	[CustomEditor(typeof(GameDataBase))]
	public class GameDataBaseEditor : Editor
	{

		GameDataBase dataBase;
		string categoryName;
		string categoryType;
		bool isCreatingCategory = false;

		public override void OnInspectorGUI()
		{
			if (dataBase == null)
			{
				dataBase = (GameDataBase) target;
			}

			base.OnInspectorGUI();
			// if (!isCreatingCategory)
			// {
			// 	if (GUILayout.Button("Create New Category"))
			// 		isCreatingCategory = true;
			// }
			// else
			// {
			// 	categoryName = EditorGUILayout.TextField("Category Name", categoryName);
			// 	categoryType = EditorGUILayout.TextField("Category Type", categoryType);
			// 	if (GUILayout.Button("Add"))
			// 	{
			// 		if (!string.IsNullOrEmpty(categoryType) && Type.GetType(categoryType) != null)
			// 		{
			// 		}
			// 		else
			// 		{
			// 			Debug.Log("it not good");
			// 		}
			// 	}
			//
			// 	if (GUILayout.Button("Cancel"))
			// 		isCreatingCategory = false;
			// }
		}

	}
	#endif


}