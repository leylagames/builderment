using UnityEngine;


namespace GameDevUtils.GameTheme
{


	[CreateAssetMenu(menuName = "GameDevUtils/GameTheme/ColorPalette")]
	public class ColorPalette : ScriptableObject
	{

		public ColorSegment   aColor;
		public ColorSegment   bColor;
		public ColorSegment   cColor;
		public ColorSegment   dColor;
		public ColorSegment   eColor;
		public ColorSegment[] optionalColors;

	}
	[System.Serializable]
	public class ColorSegment
	{

		public Color color;
		public bool  isEmission;
		public Color emissionColor;

	}

}