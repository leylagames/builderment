using System;
using UnityEngine;


namespace GameDevUtils.GameTheme
{


	public class ThemeManager : MonoBehaviour
	{

		[SerializeField]                              ColorPalette[] allColorPallets;
		[SerializeField]                              ColorPalette   themeColorPalette;
		[Header("Shared Materials")] [SerializeField] Material       aMaterial;
		[SerializeField]                              Material       bMaterial;
		[SerializeField]                              Material       cMaterial;
		[SerializeField]                              Material       dMaterial;
		[SerializeField]                              Material       eMaterial;
		[SerializeField]                              Material[]     optionalMaterials;


		public void Awake()
		{
			RefreshTheme();
		}

		private void ChangeColorPale(ColorPalette colorPalette)
		{
			themeColorPalette = colorPalette;
			RefreshTheme();
		}

		private void RefreshTheme()
		{
			if (aMaterial)
			{
				UpdateMaterial(aMaterial, themeColorPalette.aColor);
			}

			if (bMaterial)
			{
				UpdateMaterial(bMaterial, themeColorPalette.bColor);
			}

			if (cMaterial)
			{
				UpdateMaterial(cMaterial, themeColorPalette.cColor);
			}

			if (dMaterial)
			{
				UpdateMaterial(dMaterial, themeColorPalette.dColor);
			}

			if (eMaterial)
			{
				UpdateMaterial(eMaterial, themeColorPalette.eColor);
			}

			for (int i = 0; i < themeColorPalette.optionalColors.Length; i++)
			{
				UpdateMaterial(optionalMaterials[i], themeColorPalette.optionalColors[i]);
			}
		}

		private void UpdateMaterial(Material material, ColorSegment save)
		{
			var emissionColorID = Shader.PropertyToID("_EmissionColor");
			material.color = save.color;
			if (save.isEmission)
			{
				material.EnableKeyword("_EMISSION");
				material.SetColor(emissionColorID, save.emissionColor);
			}
			else
			{
				material.DisableKeyword("_EMISSION");
			}
		}

	}


}