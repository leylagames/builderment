using System;
using UnityEngine;


namespace GameDevUtils
{


	public abstract class GameDevObject : ScriptableObject
	{

		public event Action<GameDevBehaviour> OnInit;
		public event Action OnUpdate;
		public event Action OnFixedUpdate;

	}


}