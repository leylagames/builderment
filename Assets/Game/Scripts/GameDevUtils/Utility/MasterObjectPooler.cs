using System.Collections.Generic;
using GameDevUtils;
using GameDevUtils.DataBase;
using UnityEngine;

public class MasterObjectPooler : SingletonLocal<MasterObjectPooler>
{

	PoolCategory poolCategory => GameDataBase.Instance.poolCategory;
	Dictionary<string, Stack<GameObject>> poolDictionary = new Dictionary<string, Stack<GameObject>>();


	[InspectorButton("Prebake")] public bool canPrebake;

	protected override void Awake()
	{
		base.Awake();
		Init();
	}

	public void Init()
	{
		foreach (PoolElement element in poolCategory)
		{
			if (!element.prebaked)
			{
				CreateElements(element);
			}
		}
	}


	public void Prebake()
	{
		foreach (PoolElement element in poolCategory)
		{
			if (element.prebaked)
			{
				CreateElements(element);
			}
		}
	}

	void CreateElements(PoolElement poolElement)
	{
		if (poolElement.noOfElement < 1) return;
		var stack = new Stack<GameObject>();
		var parent = new GameObject(poolElement.nameId).transform;
		parent.parent = transform;
		//Fly Weight
		var runtimeObject = Instantiate(poolElement.prefab, parent);
		runtimeObject.SetActive(false);
		runtimeObject.name = $"{poolElement.nameId} {1}";
		stack.Push(runtimeObject);
		for (int i = 0; i < poolElement.noOfElement - 1; i++)
		{
			runtimeObject = Instantiate(runtimeObject, parent);
			runtimeObject.name = $"{poolElement.nameId} {i + 2}";
			runtimeObject.SetActive(false);
			stack.Push(runtimeObject);
		}

		poolDictionary.Add(poolElement.nameId, stack);
	}

	public GameObject GetNew(string nameId)
	{
		if (poolDictionary.ContainsKey(nameId))
		{
			var newObject = poolDictionary[nameId].Pop();
			newObject.SetActive(true);
			return newObject;
		}

		return null;
	}

	public T GetNew<T>(string nameId)
	{
		if (poolDictionary.ContainsKey(nameId))
		{
			var gObject = poolDictionary[nameId].Pop();
			gObject.SetActive(true);
			return gObject.GetComponent<T>();
		}

		return default(T);
	}

	//
	public T GetNewAt<T>(string nameId, Vector3 position, Quaternion rotation)
	{
		var gObject = GetNew(nameId);
		if (gObject)
		{
			gObject.transform.position = position;
			gObject.transform.rotation = rotation;
			gObject.transform.parent = null;
			return gObject.GetComponent<T>();
		}

		return default(T);
	}

	//
	public GameObject GetNewAt(string nameId, Vector3 position, Quaternion rotation)
	{
		var gObject = GetNew(nameId);
		if (gObject)
		{
			gObject.transform.position = position;
			gObject.transform.rotation = rotation;
			gObject.transform.parent = null;
			return gObject;
		}

		return null;
	}

	//
	public void Free(string nameId, GameObject gObject)
	{
		if (poolDictionary.ContainsKey(nameId))
		{
			gObject.SetActive(false);
			poolDictionary[nameId].Push(gObject);
		}
	}

}