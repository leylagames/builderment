﻿using System;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;


namespace GameDevUtils.VirtualCurrencySystem
{


	public abstract class CurrencyBinding : MonoBehaviour
	{

		[SerializeField] protected Currency currencyName;

		private void OnDestroy()
		{
			VCHandler.OnValueChangeUnregister(currencyName, ChangeEffect);
		}

		protected abstract void ChangeEffect(object sender, PropertyChangedEventArgs args);

		private void Start()
		{
			VCHandler.OnValueChangeRegister(currencyName, ChangeEffect);
		}

	}


}