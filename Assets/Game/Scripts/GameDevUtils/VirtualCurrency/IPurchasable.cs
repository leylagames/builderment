﻿using System;
using UnityEngine;


namespace GameDevUtils.VirtualCurrencySystem
{


	public interface IPurchasable
	{
		CurrencyValue[] currencyValues { get; }
		bool            IsPurchased    { get; set; }
		void            Purchased();

	}

	[Serializable]
	public struct CurrencyValue
	{

		public Currency currencyName;
		public float    price;

	}


}