﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using GameDevUtils.DataManagement;
using UnityEngine;


namespace GameDevUtils.VirtualCurrencySystem
{


	public static class VCHandler
	{
		
		static Dictionary<Currency, VirtualCurrency> virtualCurrencies;

		[RuntimeInitializeOnLoadMethod]
		public static void Initialize()
		{
			var v = Enum.GetNames(typeof(Currency));
			virtualCurrencies = new Dictionary<Currency, VirtualCurrency>(v.Length);
			foreach (string s in v)
			{
				if (Enum.TryParse(s, out Currency currency))
				{
					var virtualCurrency = new VirtualCurrency(currency, 0);
					DataManager.Get<float>(virtualCurrency);
					virtualCurrencies.Add(currency, virtualCurrency);
				}
			}
			
		}

		public static void Save()
		{
			foreach (var currency in virtualCurrencies)
			{
				DataManager.Save(currency.Value);
			}
			
		}

		public static void OnValueChangeRegister(Currency currencyName, PropertyChangedEventHandler changeEvent)
		{
			virtualCurrencies[currencyName].PropertyChanged += changeEvent;
			changeEvent.Invoke(virtualCurrencies[currencyName], null);
		}

		public static void OnValueChangeUnregister(Currency currencyName, PropertyChangedEventHandler changeEvent)
		{
			virtualCurrencies[currencyName].PropertyChanged -= changeEvent;
		}


		public static float GetValue(Currency currencyName)
		{
			return virtualCurrencies[currencyName].value;
		}

		public static float AddValue(Currency currencyName, float value)
		{
			return virtualCurrencies[currencyName].value += value;
		}

		public static void SetValue(Currency currencyName, float value)
		{
			virtualCurrencies[currencyName].value = value;
		}

		/// <summary>
		/// Buy with Specific Currency
		/// </summary>
		/// <param name="purchasable"></param>
		/// <param name="currency"></param>
		/// <param name="onPurchaseSuccess"></param>
		/// <param name="onPurchaseFailed"></param>
		/// <returns></returns>
		public static bool Buy(IPurchasable purchasable, Currency currency, Action onPurchaseSuccess, Action onPurchaseFailed)
		{
			CurrencyValue value = purchasable.currencyValues.First(c => c.currencyName == currency);
			if (virtualCurrencies[currency].value >= value.price)
			{
				virtualCurrencies[currency].value -= value.price;
				purchasable.Purchased();
				onPurchaseSuccess?.Invoke();
				return true;
			}

			onPurchaseFailed?.Invoke();
			return false;
		}

		/// <summary>
		/// Buy with Possible Currency  
		/// </summary>
		/// <param name="purchasable"></param>
		/// <param name="onPurchaseSuccess"></param>
		/// <param name="onPurchaseFailed"></param>
		/// <returns></returns>
		public static bool Buy(IPurchasable purchasable, Action onPurchaseSuccess, Action onPurchaseFailed)
		{
			foreach (var currencyValue in purchasable.currencyValues)
			{
				if (virtualCurrencies[currencyValue.currencyName].value >= currencyValue.price)
				{
					virtualCurrencies[currencyValue.currencyName].value -= currencyValue.price;
					purchasable.Purchased();
					onPurchaseSuccess?.Invoke();
					return true;
				}
			}

			onPurchaseFailed?.Invoke();
			return false;
		}

	}

	public enum Currency
	{

		Coin = 1 << 0,
		Cash = 1 << 1
		// Add New Name here like Gems = 1 << 2 

	}


}