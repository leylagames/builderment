﻿using System.ComponentModel;
using GameDevUtils.DataManagement;
using System.Runtime.CompilerServices;


namespace GameDevUtils.VirtualCurrencySystem
{


	[System.Serializable]
	public class VirtualCurrency : INotifyPropertyChanged, IDataElement
	{

		float m_value;

		public VirtualCurrency(Currency name, float initValue = 0)
		{
			Name = name;
			if (!IsAvailable)
			{
				value = initValue;
			}
		}

		public Currency Name;
		public bool     IsAvailable = false;

		public float value
		{
			get => m_value;
			set
			{
				m_value = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;


		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public string dataTag => Name.ToString();

		public Data SaveData()
		{
			return new Data<float>(value);
		}

		public void LoadData(Data data)
		{
			IsAvailable = data != null;
			if (IsAvailable)
			{
				value = ((Data<float>) data).value;
			}
			
		}

	}


}