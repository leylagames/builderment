using System.Collections.Generic;
using GameDevUtils;
using GameDevUtils.DataBase;

public class Inventory : SingletonLocal<Inventory>
{

	List<InventoryCategory> inventoryCategory => GameDataBase.Instance.inventoryCategory;
	Dictionary<string, InventoryCategory> itemCategories = new Dictionary<string, InventoryCategory>();

	protected override void Awake()
	{
		base.Awake();
		foreach (InventoryCategory category in inventoryCategory)
		{
			itemCategories.Add(category.name, category);
		}
	}

	public IList<Item> GetItems(string category)
	{
		return itemCategories[category].elements;
	}

	

}