using System.Collections;
using System.Collections.Generic;
using GameDevUtils;
using GameDevUtils.GameProgression;
using GameDevUtils.MVVM;
using GameDevUtils.UI;
using GameDevUtils.VirtualCurrencySystem;
using UnityEngine;

public class InventoryPanel : UIPanel
{

	bool isContainerUpdate = false;
	InputEditEvents inputEditEvents;

	public override void Init(IStateMachine stateMachine)
	{
		base.Init(stateMachine);
		GameController.OnGameStart.AddListener(OnGameStart);
	}

	public override void Enter()
	{
		base.Enter();
		if (isContainerUpdate) return;
		isContainerUpdate = true;
		var items = Inventory.Instance.GetItems("Construstable");
		var parent = views["Container"].GetTransform();
		EventBinder("BackButton", () =>
		{
			stateMachine.ExitAnyStates();
			inputEditEvents.onCancel.Send();
		});
		foreach (Item item in items)
		{
			var itemUI = MasterObjectPooler.Instance.GetNewAt<ItemUI>("ItemUI", parent.position, parent.rotation);
			itemUI.Setup(item.itemName, item.icon, item.currencyValues[0].price.ToInt(), () => VCHandler.Buy(item, () => PurchaseSuccess(item), () => PurchaseSuccess(item)));
			itemUI.transform.parent = parent;
			itemUI.transform.localScale = Vector3.one;
		}
	}

	void OnGameStart()
	{
		if (inputEditEvents == null)
		{
			inputEditEvents = storageService.Load<InputEditEvents>();
		}
	}


	void PurchaseSuccess(Item item)
	{
		inputEditEvents.onSelect.Send(item.placeableIndex);
	}

}