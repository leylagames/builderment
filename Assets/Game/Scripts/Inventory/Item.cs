using GameDevUtils.DataBase;
using GameDevUtils.VirtualCurrencySystem;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Inventory/Item")]
public class Item : ScriptableObject, IPurchasable
{

	public string itemName;
	public Sprite icon;
	[PlaceableIndex] public int placeableIndex = 0;
	[SerializeField] CurrencyValue[] priceValues = new CurrencyValue[] {new CurrencyValue() {currencyName = Currency.Coin, price = 10}};
	public CurrencyValue[] currencyValues => priceValues;
	public bool IsPurchased { get; set; }

	public void Purchased()
	{
	}

}


public class PlaceableIndexAttribute : PropertyAttribute
{

}
#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(PlaceableIndexAttribute))]
public class PlaceableIndexAttributeDrawer : PropertyDrawer
{

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		var placeables = GameDataBase.Instance.placeableContainer.placeables;
		var placeableNames = new string[placeables.Count];
		for (int i = 0; i < placeables.Count; i++)
		{
			placeableNames[i] = placeables[i].nameString;
		}

		property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, placeableNames);
	}

}
#endif