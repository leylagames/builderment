using System;
using GameDevUtils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{

	public void Setup(string name, Sprite icon, int amount, Action onPurchaseClick)
	{
		var iconImage = transform.FindChildByRecursion<Image>("Icon");
		var itemName = transform.FindChildByRecursion<TextMeshProUGUI>("ItemName");
		var amountText = transform.FindChildByRecursion<TextMeshProUGUI>("Amount");
		var itemUiButton = transform.FindChildByRecursion<Intractable>("ItemUiButton");
		itemUiButton.onClick.AddListener(onPurchaseClick.Invoke);
		iconImage.sprite = icon;
		itemName.text = name;
		amountText.text = amount.ToShortNumber();
	}

}