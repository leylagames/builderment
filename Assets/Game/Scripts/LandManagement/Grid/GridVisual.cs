using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridVisual : MonoBehaviour
{

	[SerializeField] Color color;
	[SerializeField] bool showDebug = true;
	GridXZ<GridElement> grid => LandManager.Instance.grid;

	void OnDrawGizmos()
	{
		if (!Application.isPlaying)
		{
			return;
		}

		if (showDebug)
		{
			Gizmos.color = color;
			for (int x = 0; x < grid.GetWidth(); x++)
			{
				for (int z = 0; z < grid.GetHeight(); z++)
				{
					Gizmos.DrawLine(grid.GetWorldPosition(x, z), grid.GetWorldPosition(x, z + 1));
					Gizmos.DrawLine(grid.GetWorldPosition(x, z), grid.GetWorldPosition(x + 1, z));
				}
			}

			Gizmos.DrawLine(grid.GetWorldPosition(0, grid.GetHeight()), grid.GetWorldPosition(grid.GetWidth(), grid.GetHeight()));
			Gizmos.DrawLine(grid.GetWorldPosition(grid.GetWidth(), 0), grid.GetWorldPosition(grid.GetWidth(), grid.GetHeight()));
			Gizmos.color = Color.white;
		}
	}

}