using System;
using GameDevUtils;
using GameDevUtils.DataManagement;

public class InputEditEvents : Data
{

	public EventListener onDelete, onRotateLeft, onRotateRight, onMove, onMultiSelect, onSave, onCancel, onTurnLeft, onTurnRight;
	public EventListener<int> onSelect;

	public InputEditEvents()
	{
		onDelete = new EventListener();
		onRotateLeft = new EventListener();
		onMove = new EventListener();
		onSelect = new EventListener<int>();
		onRotateRight = new EventListener();
		onMultiSelect = new EventListener();
		onSave = new EventListener();
		onCancel = new EventListener();
		onTurnLeft = new EventListener();
		onTurnRight = new EventListener();
	}

}