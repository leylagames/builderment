using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameDevUtils.DataBase;
using UnityEngine;


[Serializable]
public class PreSaveElement
{

	[PlaceableIndex] public int placeableIndex;
	public Vector2Int origin;
	public Direction direction;

}

[CreateAssetMenu(menuName = "Game/LandManagement/LandData", fileName = "LandData", order = 0)]
public class LandData : ScriptableObject
{

	public List<PreSaveElement> preSaveElements = new List<PreSaveElement>();

	public void Add(PlacedObject placedObject)
	{
		var element = placedObject.GetPreSaveElement();
		preSaveElements.Add(element);
	}


	public void Remove(LandManager landManager)
	{
	}

	public void Load(LandManager landManager)
	{
		Task task = new Task(() =>
		{
			foreach (PreSaveElement preSaveElement in preSaveElements)
			{
				landManager.CreatePlacedObject(GameDataBase.Instance.placeableContainer[preSaveElement.placeableIndex], preSaveElement.origin, preSaveElement.direction);
			}
		});
		task.RunSynchronously();
		task.Dispose();
	}

}