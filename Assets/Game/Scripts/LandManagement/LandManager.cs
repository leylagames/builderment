using System;
using System.Collections;
using System.Collections.Generic;
using BitBenderGames;
using GameDevUtils;
using GameDevUtils.DataBase;
using GameDevUtils.DataManagement;
using GameDevUtils.GameProgression;
using UnityEngine;

public class LandManager : SingletonLocal<LandManager>, IDataElement
{

	[SerializeField] TouchInputController inputController;
	[SerializeField] Camera camera;
	[SerializeField] LandData preLoadData;
	GridSettings gridSettings => GameDataBase.Instance.gridSettings;
	PlaceableContainer placeableObjects => GameDataBase.Instance.placeableContainer;

	public GridXZ<GridElement> grid;
	Placeable placeable;
	Direction direction;
	int placeableIndex = 0;


	InputEditEvents inputEditEvents;

	Transform ghost;

	protected override void Awake()
	{
		base.Awake();
		grid = new GridXZ<GridElement>(gridSettings.width, gridSettings.length, gridSettings.cellSize, transform.position, OnGridObjectCreation);
		InitEditEvents();
		OnGameStart();
	}

	// Load Data
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.P))
		{
			inputEditEvents.onSave.Send();
		}

		if (Input.GetKeyDown(KeyCode.O))
		{
			inputEditEvents.onCancel.Send();
		}
	}

	void LoadSavedData()
	{
	}


	void OnGameStart()
	{
		inputController.OnFingerDown += FingerDown;
		inputController.OnFingerUp += FingerUp;
		inputController.OnDragUpdate += DragUpdate;
		inputController.OnFingerDown += _ => FingerUp();
		preLoadData.Load(this);
	}


	#region InitEditEvents

	void InitEditEvents()
	{
		var storageService = LocalStorageService.GetService();
		inputEditEvents = new InputEditEvents();
		storageService.Save(inputEditEvents);
		inputEditEvents.onDelete.AddListener(OnDelete);
		inputEditEvents.onMove.AddListener(OnMove);
		inputEditEvents.onRotateLeft.AddListener(OnRotateLeft);
		inputEditEvents.onRotateRight.AddListener(OnRotateRight);
		inputEditEvents.onSelect.AddListener(OnSelect);
		inputEditEvents.onCancel.AddListener(OnCancel);
		inputEditEvents.onSave.AddListener(OnSave);
	}

	void OnCancel()
	{
		ResetPlaceable();
	}

	void ResetPlaceable()
	{
		placeable = null;
		if (ghost != null)
			Destroy(ghost.gameObject);
	}

	void OnSave()
	{
		CreatePlaceableObject();
	}

	void OnDelete()
	{
	}

	void OnMove()
	{
		int a=10, b=5;
		(a, b) = (b, a);
	}

	void OnRotateLeft()
	{
		direction = Placeable.GetPreviousDirection(direction);
	}

	void OnRotateRight()
	{
		direction = Placeable.GetNextDirection(direction);
	}

	void OnSelect(int index)
	{
		placeableIndex = index;
		placeable = placeableObjects.placeables[placeableIndex];
		if (ghost != null)
		{
			Destroy(ghost.gameObject);
		}

		ghost = Instantiate(placeable.visual, transform);
	}

	#endregion


	void DragUpdate(Vector3 start, Vector3 current, Vector3 offset)
	{
	}

	void FingerUp()
	{
	}

	void FingerDown(Vector3 pos)
	{
		if (placeable != null) return;
		if (GetMouseWorldPosition(out RaycastHit raycastHit))
		{
			var placedObject = raycastHit.collider.GetComponent<PlacedObject>();
			if (placedObject)
			{
				placeable = placedObject.GetPlaceableObject();
				if (placeable == null || placeable.type == PlaceableType.NoneEditable) return;
				if (ghost != null)
				{
					Destroy(ghost);
				}

				direction = placedObject.GetDirection();
				ghost = Instantiate(placeable.visual, transform);
				ghost.position = placedObject.transform.position;
				Destroy(placedObject.gameObject);
				inputEditEvents.onSelect.Send(placedObject.GetPlaceableObjectIndex());
			}
		}
	}

	void CreatePlaceableObject()
	{
		if (!placeable) return;
		Vector3 mousePosition = GetMouseWorldPosition();
		grid.GetXZ(mousePosition, out int x, out int z);
		Vector2Int placedObjectOrigin = new Vector2Int(x, z);
		CreatePlacedObject(placeable, placedObjectOrigin, this.direction);
		ResetPlaceable();
	}

	public void CreatePlacedObject(Placeable placeable, Vector2Int placedObjectOrigin, Direction direction)
	{
		placedObjectOrigin = grid.ValidateGridPosition(placedObjectOrigin);
		List<Vector2Int> gridPositionList = placeable.GetGridPositionList(placedObjectOrigin, direction);
		bool canBuild = true;
		foreach (Vector2Int gridPosition in gridPositionList)
		{
			if (!grid.GetGridObject(gridPosition.x, gridPosition.y).CanBuild())
			{
				canBuild = false;
				break;
			}
		}

		if (canBuild)
		{
			Vector2Int rotationOffset = placeable.GetRotationOffset(direction);
			Vector3 placedObjectWorldPosition = grid.GetWorldPosition(placedObjectOrigin.x, placedObjectOrigin.y) + new Vector3(rotationOffset.x, 0, rotationOffset.y) * grid.GetCellSize();
			var placedObject = PlacedObject.Create(placedObjectWorldPosition, placedObjectOrigin, direction, placeable);
			foreach (Vector2Int gridPosition in gridPositionList)
			{
				grid.GetGridObject(gridPosition.x, gridPosition.y).SetPlacedObject(placedObject, placeable.type == PlaceableType.Walkable);
			}

			placedObject.transform.parent = transform;
			//	OnObjectPlaced?.Invoke(this, EventArgs.Empty);

			//DeselectObjectType();
		}
	}

	Vector3 GetMouseWorldSnappedPosition()
	{
		Vector3 mousePosition = GetMouseWorldPosition();
		grid.GetXZ(mousePosition, out int x, out int z);
		if (placeable != null)
		{
			Vector2Int rotationOffset = placeable.GetRotationOffset(direction);
			Vector3 placedObjectWorldPosition = grid.GetWorldPosition(x, z) + new Vector3(rotationOffset.x, 0, rotationOffset.y) * grid.GetCellSize();
			return placedObjectWorldPosition;
		}

		return mousePosition;
	}

	public Quaternion GetPlacedObjectRotation()
	{
		if (placeable != null)
		{
			return Quaternion.Euler(0, placeable.GetRotationAngle(direction), 0);
		}

		return Quaternion.identity;
	}

	Vector3 GetMouseWorldPosition()
	{
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		return Physics.Raycast(ray, out RaycastHit raycastHit, 999f) ? raycastHit.point : Vector3.zero;
	}

	bool GetMouseWorldPosition(out RaycastHit raycastHit)
	{
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		return Physics.Raycast(ray, out raycastHit, 999f);
	}

	GridElement OnGridObjectCreation(GridXZ<GridElement> grid, int x, int y)
	{
		return new GridElement(grid, x, y);
	}

	private void LateUpdate()
	{
		MoveGhost();
	}

	void MoveGhost()
	{
		if (ghost == null) return;
		Vector3 targetPosition = GetMouseWorldSnappedPosition();
		targetPosition.y = 1f;
		ghost.position = Vector3.Lerp(ghost.position, targetPosition, Time.deltaTime * 15f);
		ghost.rotation = Quaternion.Lerp(ghost.rotation, GetPlacedObjectRotation(), Time.deltaTime * 15f);
	}


	public void SaveToPreSaveData(PlacedObject placedObject)
	{
		preLoadData.Add(placedObject);
	}


	// Data save system
	Data landData;
	public string dataTag => nameof(LandManager);

	public Data SaveData()
	{
		return landData;
	}

	public void LoadData(Data data)
	{
		landData = data;
	}

}

public class GridElement
{

	private GridXZ<GridElement> grid;
	private int x;
	private int y;
	public PlacedObject placedObject;
	private bool walkable;

	public GridElement(GridXZ<GridElement> grid, int x, int y)
	{
		this.grid = grid;
		this.x = x;
		this.y = y;
		placedObject = null;
	}

	public override string ToString()
	{
		return x + ", " + y + "\n";
	}

	public void SetPlacedObject(PlacedObject placedObject, bool walkale)
	{
		this.placedObject = placedObject;
		this.walkable = walkale;
		grid.TriggerGridObjectChanged(x, y);
	}

	public void ClearPlacedObject()
	{
		placedObject = null;
		grid.TriggerGridObjectChanged(x, y);
	}

	public PlacedObject GetPlacedObject()
	{
		return placedObject;
	}

	public bool Walkable()
	{
		return walkable;
	}

	public bool CanBuild()
	{
		return placedObject == null;
	}

}