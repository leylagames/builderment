using System.Collections;
using System.Collections.Generic;
using GameDevUtils.DataBase;
using UnityEngine;

public class PlacedObject : MonoBehaviour
{

	private Placeable placeableObject;
	private Vector2Int origin;
	private Direction direction;

	public Placeable GetPlaceableObject()
	{
		return placeableObject;
	}

	public Direction GetDirection() => direction;

	public int GetPlaceableObjectIndex()
	{
		return GameDataBase.Instance.placeableContainer.IndexOf(placeableObject);
	}

	protected virtual void Setup(Placeable placeable, Vector2Int origin, Direction direction)
	{
		this.placeableObject = placeable;
		this.origin = origin;
		this.direction = direction;
	}

	public PreSaveElement GetPreSaveElement()
	{
		PreSaveElement element = new PreSaveElement();
		element.placeableIndex = GetPlaceableObjectIndex();
		element.direction = direction;
		element.origin = origin;
		return element;
	}

	public List<Vector2Int> GetGridPositionList()
	{
		return placeableObject.GetGridPositionList(origin, direction);
	}

	public void DestroySelf()
	{
		Destroy(gameObject);
	}

	public override string ToString()
	{
		return placeableObject.nameString;
	}


	public static PlacedObject Create(Vector3 worldPosition, Vector2Int origin, Direction dir, Placeable placeableObject)
	{
		Transform placedObjectTransform = Instantiate(placeableObject.prefab, worldPosition, Quaternion.Euler(0, placeableObject.GetRotationAngle(dir), 0));
		PlacedObject placedObject = placedObjectTransform.GetComponent<PlacedObject>();
		placedObject.Setup(placeableObject, origin, dir);
		return placedObject;
	}


	[InspectorButton("SaveToPreSaveData")] public bool canSave = false;

	public void SaveToPreSaveData()
	{
		LandManager.Instance.SaveToPreSaveData(this);
	}

}