using UnityEngine;

[CreateAssetMenu(menuName ="Game/LandManagement/RotatePlaceable", fileName = "RotatePlaceable")]
public class RotatePlaceable : Placeable
{

	public Transform[] rotationPrefabs;
	public Transform[] rotationGhostPrefabs;

	public Transform GetPrefab(Direction direction)
	{
		switch (direction)
		{
			default:
			case Direction.Down: return prefab;

			case Direction.Left: return rotationPrefabs[0];

			case Direction.Up: return rotationPrefabs[1];

			case Direction.Right: return rotationPrefabs[2];
		}
	}

	public Transform GetGhost(Direction direction)
	{
		switch (direction)
		{
			default:
			case Direction.Down: return visual;

			case Direction.Left: return rotationGhostPrefabs[0];

			case Direction.Up: return rotationGhostPrefabs[1];

			case Direction.Right: return rotationGhostPrefabs[2];
		}
	}

}